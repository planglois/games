;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages human-resource-machine)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public gog-human-resource-machine
  (let ((gog-version "2.0.0.3")
        (binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "HumanResourceMachine.bin.x86_64")
                  ("i686-linux" "HumanResourceMachine.bin.x86")
                  (_ ""))))
    (package
      (name "gog-human-resource-machine")
      (version "1.0.8262")
      (source
       (origin
        (method gog-fetch)
        (uri "gogdownloader://human_resource_machine/en3installer2")
        (file-name (string-append "gog_human_resource_machine_"
                                  gog-version ".sh"))
        (sha256
         (base32
          "0s91gcdn8wqakpxlaw16q6hf608r9h56kx7imd6khhj2pxpxsd7d"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "gcc:lib" "libstdc++" "mesa" "openal" "sdl2" "zlib")))
         #:phases
         (modify-phases %standard-phases
           (add-before 'install 'delete-bundled-libs
             (lambda _
               (delete-file-recursively "data/noarch/game/lib")
               (delete-file-recursively "data/noarch/game/lib64")
               #t)))))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("libstdc++" ,(make-libstdc++ gcc))
         ("mesa" ,mesa)
         ("openal" ,openal)
         ("sdl2" ,sdl2)
         ("zlib" ,zlib)))
      (home-page "https://tomorrowcorporation.com/humanresourcemachine")
      (synopsis "Program little office workers to solve puzzles")
      (description "Human Resource Machine is a puzzle game for nerds.  In each
level, your boss gives you a job.  Automate it by programming your little
office worker.  If you succeed, you'll be promoted up to the next level for
another year of work in the vast office building.  Congratulations!

Don't worry if you've never programmed before - programming is just puzzle
solving.  If you strip away all the 1's and 0's and scary squiggly brackets,
programming is simple, logical, beautiful, and something that anyone can
understand and have fun with!  Are you already an expert?  There will be extra
challenges for you.

Have fun!  Management is watching.

@itemize
@item
Learn to program inside a giant computer made of humans.  You'll be taught
everything you need to know.
@item
Already an expert? Each level comes with Optimization Challenges - difficult
(and optional) challenges that test how well your solution optimizes for
program size and execution speed.
@item
From the creators of Little Inferno and World of Goo.
@end itemize")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
